import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PopupDialog } from "./components/popup-dialog/popup-dialog"
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular8-test';


  tabs: any;

  departmentName: string;
  name: string;

  navLinks: any[];
  activeLinkIndex = -1;
  constructor(private router: Router, public dialog: MatDialog) {
    this.tabs = localStorage['tabs'] ? JSON.parse(localStorage['tabs']) : ['physics', 'Computer'];
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PopupDialog, {
      width: '250px',
      data: { name: this.name, departmentName: this.departmentName }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed',result);
      this.departmentName = result;
      this.addTab(this.departmentName);
    });
  }


  addTab(tabName: string) {
    if (tabName == "" || tabName == undefined) {
      return;
    }


    this.tabs.push(tabName);
    localStorage["tabs"]=JSON.stringify(this.tabs);
  }

  removeTab(index: number) {
    this.tabs.splice(index, 1);
    localStorage["tabs"]=JSON.stringify(this.tabs);

  }
  getLabel(depName) {
    let count = 0;
    if (localStorage[depName + ""]) {
      count = JSON.parse(localStorage[depName + ""]).length - 1
    }
    return depName + "(" + count + ")";
  }



}
