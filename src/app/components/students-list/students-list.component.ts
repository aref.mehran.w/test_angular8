import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})




export class StudentsListComponent {


 
  studentArray= [
    {  name: 'mehran',  lastName: 'arefkhani', birthDate: "10/5/1990",editable:false },
    {  name: 'mohamad',  lastName: 'hoseint', birthDate: "10/25/1990" ,editable:false},
    {  name: '',  lastName: '', birthDate: '' ,editable:true,isLast:true},  
  ];

  
  displayedColumns = [ 'name', 'lastName', 'birthDate'];
  dataSource = new MatTableDataSource(this.studentArray);

  beforEditElement:any;

  depName:string;

  constructor(private route: ActivatedRoute) {}


  updateData()
  {
    this.dataSource._updateChangeSubscription(); // <-- Refresh the datasource
    localStorage[this.depName+""]=JSON.stringify(this.dataSource.data);

  }
  deleteClicked(element)
  {
    let index=this.dataSource.filteredData.indexOf(element);
    console.log(element,index);
    this.dataSource.data.splice(index, 1);
    this.updateData()


  }

  editClicked(element)
  {

    this.beforEditElement={...element};
    let index=this.dataSource.filteredData.indexOf(element);
    console.log(element,index);

    for(var el of this.dataSource.data)
    {
      if(!el.isLast){
        el.editable=false;
      }else{
        el.editable=true;
      }
        
    }
    this.dataSource.data[index].editable=true;

    this.updateData()


  }

  editConfirmed(element)
  {
    let index=this.dataSource.filteredData.indexOf(element);
    console.log(element,index);
    this.dataSource.data[index].editable=false;

    this.updateData()


  }


  editCanceled(element)
  {
    let index=this.dataSource.filteredData.indexOf(element);
    this.dataSource.data[index]={...this.beforEditElement};
    this.updateData()
  }

  addClicked(element)
  {
    for(var el of this.dataSource.data)
    {
        el.isLast=false;
        el.editable=false;
    }
    this.dataSource.data.push({  name: '',  lastName: '', birthDate: '' ,editable:true,isLast:true});

    this.updateData()


  }

  ngOnInit() {
    this.route.params.subscribe(params => {
       this.depName = params['depName']; // (+) converts string 'id' to a number
       let storageData=localStorage[this.depName+""]
       let emptyRow=[{  name: '',  lastName: '', birthDate: '' ,editable:true,isLast:true}];
       var data=storageData ? JSON.parse(storageData) : emptyRow
       this.dataSource.data=data 
       this.updateData();
    });
  }




}


/**  Copyright 2018 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */
